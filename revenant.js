let Database = {
  data: null,
  error: "",
  fetch: () => {
    if (Database.data != null) {
      m.request({
        method: "GET",
        url: "/revenant.json",
      }).then((res) => {
        Database.data = res;
      }).catch((err) => {
        Database.error = err;
      });
    }
  }
};

class Home {
  constructor(vnode) {}

  oninit(vnode) {
    Database.fetch();
  }

  view({ attrs }) {
    return [
      m("select", {
        oninput: () => {},
      }, [
        Object.keys(Database.data ? Database.data : {}).forEach((key, index) => {
          return m("option", {
            value: key.toUpperCase()
          }, key.toUpperCase());
        })
      ]),
      m("p", ),
    ];
  }
}

class Item {
  constructor(vnode) {}

  oninit(vnode) {
    Database.fetch();
  }

  view({ attrs }) {
    return [
      m("div"),
    ];
  }
}

m.route(document.body, "/", {
  "/": Home,
  "/:type/:title": Item,
});